package com.nespresso.exercise.dockyard;

public class Dockyard implements Loadable, UnLoadable {

	private int depotSize;
	private int payload;

	private Ship shipSource;
	private Ship shipDestination;

	private Elevator elevator;

	public Dockyard(int depotSize) {
		this.depotSize = depotSize;
		this.elevator = new Elevator();
		shipSource = new NullShip();
		shipDestination = new NullShip();
	}

	public void sourceShip(int payload) {
		shipSource = new Ship(payload, payload);
		elevator.setElevatorAction(new UnloadElevatorAction(shipSource, this));
	}

	public void destinationShip(int cargoCapacity) {
		shipDestination = new Ship(cargoCapacity);
		elevator.setElevatorAction(new LoadElevatorAction(shipSource,
				shipDestination, this));
	}

	public String print() {
		IPrinter printer = new TextPrinter();
		shipSource.print(printer);
		printer.append(payload);
		shipDestination.print(printer);
		return printer.print();
	}

	public void workForOneHour() {
		elevator.work();
	}

	public void load() {
		payload++;
	}

	public boolean canLoad() {
		return payload < depotSize;
	}

	public void unload() {
		payload--;
	}

	public boolean canUnload() {
		return payload > 0;
	}
}
