package com.nespresso.exercise.dockyard;

public interface Loadable {

	void load();
	
	boolean canLoad();
}
