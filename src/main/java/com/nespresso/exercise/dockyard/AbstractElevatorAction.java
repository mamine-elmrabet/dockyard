package com.nespresso.exercise.dockyard;

public abstract class AbstractElevatorAction {

	protected Ship ship;
	protected Dockyard dockyard;

	public AbstractElevatorAction(Ship ship, Dockyard dockyard) {
		this.ship = ship;
		this.dockyard = dockyard;
	}

	public abstract void work();

//	public abstract int[][] getStatus();

}
