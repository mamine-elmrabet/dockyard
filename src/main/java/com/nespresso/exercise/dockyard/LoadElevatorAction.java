package com.nespresso.exercise.dockyard;

public class LoadElevatorAction extends AbstractElevatorAction {

	private Ship shipDestination;

	public LoadElevatorAction(Ship shipSource, Ship shipDestination,
			Dockyard dockyard) {
		super(shipSource, dockyard);
		this.shipDestination = shipDestination;
	}

	public void work() {
		if (ship.canUnload() && dockyard.canLoad()) {
			ship.unload();
			dockyard.load();
		}
		if (dockyard.canUnload() && shipDestination.canLoad()) {
			dockyard.unload();
			shipDestination.load();
		}
	}

//	@Override
//	public int[][] getStatus() {
//
//		int payloadShipSource = ship.getStatus();
//		int payloadDockyard = dockyard.getStatus();
//		int payloadShipDestination = shipDestination.getStatus();
//
//		int maxPaykoad = Math.max(payloadDockyard,
//				Math.max(payloadShipSource, payloadShipDestination));
//
//		int data[][] = new int[maxPaykoad][5];
//		int i = maxPaykoad - 1;
//		while (i >= 0) {
//			if (i >= maxPaykoad - payloadShipSource)
//				data[i][0] = 1;
//			if (i >= maxPaykoad - payloadDockyard)
//				data[i][2] = 1;
//			if (i >= maxPaykoad - payloadShipDestination)
//				data[i][4] = 1;
//			i--;
//		}
//		return data;
//	}
}
