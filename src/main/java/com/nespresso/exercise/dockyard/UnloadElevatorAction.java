package com.nespresso.exercise.dockyard;

public class UnloadElevatorAction extends AbstractElevatorAction {

	public UnloadElevatorAction(Ship ship, Dockyard dockyard) {
		super(ship, dockyard);
	}

	public void work() {
		if (ship.canUnload() && dockyard.canLoad()) {
			ship.unload();
			dockyard.load();
		}
	}

//	@Override
//	public int[][] getStatus() {
//
//		int payloadShipSource = ship.getStatus();
//		int payloadDockyard = dockyard.getStatus();
//
//		int maxPaykoad = Math.max(payloadDockyard, payloadShipSource);
//
//		int data[][] = new int[maxPaykoad][5];
//		int i = maxPaykoad - 1;
//		while (i >= 0) {
//			if (i >= maxPaykoad - payloadShipSource)
//				data[i][0] = 1;
//			if (i >= maxPaykoad - payloadDockyard)
//				data[i][2] = 1;
//			i--;
//		}
//		return data;
//	}
}
