package com.nespresso.exercise.dockyard;

public interface UnLoadable {

	void unload();
	boolean canUnload();
}
